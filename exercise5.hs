as [] = []
as [x] = [[[x]]]
as (x:xs) = map ([x]:) axs ++ map (\(y:ys) -> (x:y):ys) axs where axs = as xs
