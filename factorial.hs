factorial 0 = 1
factorial n = n * factorial(n-1)

onelinefactorial n = if n>0 then n*onelinefactorial(n-1) else 1

withoutstackoverflow n = let internal 0 y = y
                             internal x y = internal (x-1) (y*x)
                         in internal n 1

anotherway n = internal n 1
                   where internal 0 y = y
                         internal x y = internal (x-1) (y*x)

simpledefinition n = product [1..n]
