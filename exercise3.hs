allEven x = filter even x

allEven2 [] = []
allEven2 (x:xs) = if even x then x:allEven2(xs) else allEven2(xs)

myBig x y = if x>y then x else y
myMax [] = 0
myMax [x] = x
myMax (x:xs) = myBig x (myMax xs)
