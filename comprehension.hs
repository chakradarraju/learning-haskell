a1 = [x^2 | x <- [1..20], even x]
a2 = [ if x < 10 then "BOOM" else "BANG" | x <- [1..25], odd x, x < 20]
a3 = [x*y | x <- [1..5], y <- [1..5], x < 2, y > 4]
