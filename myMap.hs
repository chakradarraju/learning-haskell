myMap fn [] = []
myMap fn (x:xs) = fn x:myMap fn xs

myMap2 fn x = foldr (\y z -> (fn y):z) [] x
