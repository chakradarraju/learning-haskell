a1 = sum [ x | x <- [1..999], mod x 3 == 0 || mod x 5 == 0 ]
fibs = 0 : 1 : [ a + b | (a, b) <- zip fibs (tail fibs) ]
a2 = sum [ x | x <- takeWhile (<4000000) fibs2, even x ]

fibs2 = let fn a b = (a+b):fn b (a+b)
        in fn 0 1
