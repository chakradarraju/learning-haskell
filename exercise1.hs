myLen [] = 0
myLen (x:xs) = 1+myLen xs

--myLen2 [] = 0
--myLen2 x = 1+myLen2 tail x

myInit [] = error "Empty list"
myInit l = if(length l == 1) then [] else((head l):(myInit (tail l)))

myInit2 l = take (length l-1) l

myLast a = if (tail a)==[] then [head a] else myLast(tail a)
