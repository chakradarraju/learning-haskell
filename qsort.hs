qsort [] = []

qsort (x:xs) = let lt = qsort([ y | y <- xs, y < x])
                   gt = qsort([ y | y <- xs, y >= x])
               in lt ++ [x] ++ gt
