sqr x = x*x
sq x = map sqr x

s x = map s2 x
      where s2 x = x*x

s3 x = map (\x -> x*x) x

s4 x = map (^2) x

s5 = map (^2)
